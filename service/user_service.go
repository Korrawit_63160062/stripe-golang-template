// services/user_service.go

package service

import (
	"bookshop/domain/repositories"
	"errors"
	"fmt"
)

type UserService struct {
	UserRepository repositories.IUserRepository
}

type IUserService interface {
	UpdateUserCredits(userID string, credits int) error
}

func NewUserService(repo repositories.IUserRepository) IUserService {
	return &UserService{
		UserRepository: repo,
	}
}

func (ser *UserService) UpdateUserCredits(userID string, credits int) error {
	// Check if the user exists
	user, err := ser.UserRepository.GetUserByID(userID)
	if err != nil {
		return err
	}
	fmt.Println(user)
	if user == nil {
		return errors.New("user not found")
	}

	// Update the credits value
	user.Credits += credits

	// Print user details after updating credits
	fmt.Printf("User Updated:\nID: %s\nUsername: %s\nEmail: %s\nCredits: %d\n",
		user.UserID, user.Username, user.Email, user.Credits)

	err = ser.UserRepository.UpdateUser(user)
	if err != nil {
		return err
	}

	return nil
}
