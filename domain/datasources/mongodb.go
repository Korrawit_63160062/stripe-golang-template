package datasources

import (
	"context"
	"log"

	"go.elastic.co/apm/module/apmmongo"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoDB struct {
	Context context.Context
	MongoDB *mongo.Client
}

type IMongoDB interface {
	Disconnect()
	Ping()
}

func NewMongoDB(maxPoolSize uint64) *MongoDB {

	option := options.Client().ApplyURI("mongodb+srv://botnoi:8H2dzgVPOOYgP2eU@cluster0.8nklu.mongodb.net/admin?retryWrites=true&w=majority&ssl=true&ssl_cert_reqs=CERT_NONE").SetMonitor(apmmongo.CommandMonitor()).SetMaxPoolSize(maxPoolSize)
	client, err0 := mongo.Connect(context.Background(), option)

	if err0 != nil {
		log.Fatal("error connection : ", err0)
	}

	return &MongoDB{
		Context: context.Background(),
		MongoDB: client,
	}
}

func (ds MongoDB) Disconnect() {
	ds.MongoDB.Disconnect(ds.Context)
}

func (ds MongoDB) Ping() {

}
