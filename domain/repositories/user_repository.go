// repositories/user_repository.go

package repositories

import (
	. "bookshop/domain/datasources"
	"bookshop/domain/entities"
	"context"
	"errors"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type userRepository struct {
	Context    context.Context
	Collection *mongo.Collection
}

type IUserRepository interface {
	UpdateUser(user *entities.User) error
	GetUserByID(userID string) (*entities.User, error)
}

func UserRepository(db *MongoDB) IUserRepository {
	return &userRepository{
		Context:    db.Context,
		Collection: db.MongoDB.Database("tts-payment").Collection("users"),
	}
}

func (repo *userRepository) UpdateUser(user *entities.User) error {
	filter := bson.M{"_id": user.ID}
	update := bson.M{"$set": user}

	result, err := repo.Collection.UpdateOne(repo.Context, filter, update)
	if err != nil {
		return err
	}

	if result.ModifiedCount == 0 {
		return errors.New("no user updated")
	}

	return nil
}

func (repo *userRepository) GetUserByID(userID string) (*entities.User, error) {
	var user entities.User

	filter := bson.M{"user_id": userID}

	err := repo.Collection.FindOne(repo.Context, filter).Decode(&user)
	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			return nil, nil // User not found
		}
		return nil, err
	}

	return &user, nil
}
