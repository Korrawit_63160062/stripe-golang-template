package entities

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type User struct {
	ID             primitive.ObjectID `bson:"_id,omitempty"`
	UserID         string             `bson:"user_id"`
	Username       string             `bson:"username"`
	Email          string             `bson:"email"`
	Image          string             `bson:"image"`
	JWT            string             `bson:"jwt"`
	StartDatetime  time.Time          `bson:"start_datetime"`
	EndDatetime    time.Time          `bson:"end_datetime"`
	InviteCode     bool               `bson:"invite_code"`
	Credits        int                `bson:"credits"`
	Agreement      bool               `bson:"agreement"`
	SignInProvider string             `bson:"sign_in_provider"`
	PersonalForm   bool               `bson:"personal_form"`
	AccountStatus  string             `bson:"account_status"`
	Token          string             `bson:"token"`
	UID            string             `bson:"uid"`
	PlayQuota      int                `bson:"play_quota"`
	Subscription   string             `bson:"subscription"`
	MonthlyPoint   int                `bson:"monthly_point"`
	UserCanvaID    string             `bson:"user_canva_id"`
	SaleCodeName   string             `bson:"sale_code_name"`
	SaleRank       string             `bson:"sale_rank"`
}

type AllUser struct {
	UserID      string    `bson:"user_id"`
	Username    string    `bson:"username"`
	Email       string    `bson:"email"`
	Credits     int       `bson:"credits"`
	EndDatetime time.Time `bson:"end_datetime"`
	UID         string    `bson:"uid"`
}
