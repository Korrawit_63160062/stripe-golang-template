package main

import (
	ds "bookshop/domain/datasources"
	"bookshop/domain/repositories"
	"bookshop/service"
	"encoding/json"
	"fmt"
	"log"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/stripe/stripe-go/v76"
	"github.com/stripe/stripe-go/v76/checkout/session"
	"github.com/stripe/stripe-go/v76/webhook"
)

// IUserService interface definition
type IUserService interface {
	UpdateUserCredits(userID string, credits int) error
}

func main() {
	// Set your Stripe secret key
	stripe.Key = "sk_test_51OJSGcFd4s6Bh2ywq3lwIL095aN4TFnUGJsbS2ytz6Gkr9jAlHbCWCCKJP9xlO6O79wAuilsnniNcuIiZ5FKNvt300ig8b92s3"
	mongodb := ds.NewMongoDB(10)
	// Create repositories and services
	userRepo := repositories.UserRepository(mongodb)
	userService := service.NewUserService(userRepo)

	// Create Fiber app
	app := fiber.New()

	// Enable CORS using middleware
	app.Use(cors.New())

	app.Post("/get_checkout_session", func(c *fiber.Ctx) error {
		// Parse the JSON request body
		var requestBody map[string]string
		if err := c.BodyParser(&requestBody); err != nil {
			log.Printf("Error parsing request body: %v", err)
			return c.Status(fiber.StatusBadRequest).SendString("Invalid request")
		}

		// Extract the client reference ID from the request body
		clientReferenceId := requestBody["clientReferenceId"]

		// Create a Checkout session with the client reference ID
		params := &stripe.CheckoutSessionParams{
			PaymentMethodTypes: stripe.StringSlice([]string{"card", "promptpay"}),
			LineItems: []*stripe.CheckoutSessionLineItemParams{
				{
					// Provide the exact Price ID (for example, pr_1234) of the product you want to sell
					Price:    stripe.String("price_1OKvfiFd4s6Bh2ywurWo7jAJ"),
					Quantity: stripe.Int64(1),
				},
			},
			Mode:              stripe.String(string(stripe.CheckoutSessionModePayment)),
			SuccessURL:        stripe.String("http://127.0.0.1:5500/success.html"),
			CancelURL:         stripe.String("http://127.0.0.1:5500/cancel.html"),
			ClientReferenceID: stripe.String(clientReferenceId), // Set the client reference ID from the request body
		}

		session, err := session.New(params)
		if err != nil {
			log.Printf("Stripe Checkout session error: %v", err)
			return c.Status(fiber.StatusInternalServerError).SendString("Payment failed")
		}

		// Return the session ID as JSON
		return c.JSON(fiber.Map{"sessionId": session.ID})
	})

	// Define the endpoint to receive Stripe webhook events
	app.Post("/webhook", func(c *fiber.Ctx) error {
		// Retrieve the request payload
		payload := c.Body()

		// Verify the webhook signature
		endpointSecret := "whsec_V1S1rWBoKmVorKNjJgB8PLJ3UFKi5gRj"
		event, err := webhook.ConstructEvent(payload, c.Get("Stripe-Signature"), endpointSecret)
		if err != nil {
			log.Printf("Webhook signature verification failed: %v", err)
			return c.SendStatus(fiber.StatusBadRequest)
		}

		switch event.Type {
		case "checkout.session.completed":
			var session stripe.CheckoutSession
			err := json.Unmarshal(event.Data.Raw, &session)
			if err != nil {
				log.Printf("Error decoding CheckoutSession: %v", err)
				return c.SendStatus(fiber.StatusInternalServerError)
			}

			// Handle checkout.session.completed event
			handleCheckoutSessionCompleted(session, userService)
		default:
			fmt.Printf("Unhandled event type: %s\n", event.Type)
		}

		// Acknowledge receipt of the event
		c.SendStatus(fiber.StatusOK)
		return nil
	})

	// Start the server
	log.Fatal(app.Listen(":8080"))
}

func handleCheckoutSessionCompleted(session stripe.CheckoutSession, userService IUserService) {
	// Access and print the client_reference_id
	clientReferenceID := session.ClientReferenceID
	fmt.Printf("Client Reference ID: %s\n", clientReferenceID)

	// Call UpdateUserCredits with credits value = 1
	userService.UpdateUserCredits(clientReferenceID, 1)
}
